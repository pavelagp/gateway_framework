package entities

import "time"

type terminal struct {
	ID               int               `json:"id"`
	CreatedAt        time.Time         `json:"created_at"`
	Name             string            `json:"name"`
	Settings         map[string]string `json:"settings"`
	GatewayActivator string            `json:"gateway_activator"`
}

type transaction struct {
	ID                       int               `json:"id"`
	CreatedAt                time.Time         `json:"created_at"`
	Description              string            `json:"description"`
	Amount                   int               `json:"amount"`
	CustomerCommissionAmount int               `json:"customer_commission_amount"`
	MerchantCommissionAmount int               `json:"merchant_commission_amount"`
	TerminalCommissionAmount int               `json:"terminal_commission_amount"`
	Metadata                 map[string]string `json:"metadata,omitempty"`
	Status                   int               `json:"transaction_status_id"`
	TransactionTypeID        int               `json:"transaction_type_id"`
	PaymentMethodID          int               `json:"payment_method_id"`
	PaymentMaskedData        string            `json:"payment_masked_data"`
	CustomerData             map[string]string `json:"customer_data,omitempty"`
	ExternalID               string            `json:"external_id"`
	SerialNumber             string            `json:"serial_number"`
}

type ProcessingRequest struct {
	Transaction transaction       `json:"transaction"`
	Terminal    terminal          `json:"terminal"`
	PaymentData map[string]string `json:"-"`
	GatewayData map[string]string `json:"gateway_data,omitempty"`
}
