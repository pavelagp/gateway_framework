package entities

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

type PaymentData struct {
	PaymentMethodID int               `json:"payment_method_id"`
	PaymentDetails  map[string]string `json:"payment_details"`
}

func (p *PaymentData) GetFromStorage(id int) (err error) {
	var (
		res  *http.Response
		req  *http.Request
		body []byte
	)
	url := os.Getenv("PS_HOST")
	token := os.Getenv("PS_TOKEN")

	req, err = http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	q := req.URL.Query()
	q.Add("id", strconv.Itoa(id))
	req.URL.RawQuery = q.Encode()

	req.Header.Add("Authorization", "Basic "+token)

	res, err = http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	if res.StatusCode != http.StatusOK {
		err = errors.New("Protected storage error: " + res.Status)
		return
	}
	defer res.Body.Close()
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, p)
	if err != nil {
		return
	}
	return
}
