package utilities

import (
	"encoding/json"
	"log"
)

type record struct {
	StatusCode    int    `json:"status_code"`
	Text          string `json:"text"`
	TransactionId int    `json:"transaction_id"`
}

var instanceTrace *tracer = nil

type tracer struct {
	buf   chan record
	topic string
}

func Tracer() *tracer {
	var err error
	if instanceTrace == nil {
		instanceTrace = new(tracer)
		instanceTrace.buf = make(chan record, 100)
		instanceTrace.topic = "tracer"
		if err != nil {
			panic(err)
		}
		go instanceTrace.run()
	}
	return instanceTrace
}

func (t *tracer) WriteTrace(transactionId int, code int, text string) {
	message := record{StatusCode: code, Text: text, TransactionId: transactionId}
	log.Println(message)
	t.buf <- message
}

func (t *tracer) run() {
	for {
		rec := <-t.buf
		marshaledRecord, err := json.Marshal(rec)
		if err != nil {
			log.Println(err)
			continue
		}
		MessageBroker().PutMessage(t.topic, string(marshaledRecord), 0)
	}
}
