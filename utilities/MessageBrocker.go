package utilities

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"os"
)

var instanceMq *messageBrocker = nil

type messageBrocker struct {
	url    string
	key    string
	secret string
	client *sqs.SQS
}

func (mq *messageBrocker) buildClient() error {
	sess, err := session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(mq.key, mq.secret, ""),
		Region:      aws.String(os.Getenv("SQS_REGION")),
		Endpoint:    aws.String(os.Getenv("SQS_ROOT")),
	})
	if err != nil {
		return err
	}
	mq.client = sqs.New(sess)
	return nil
}

func (mq *messageBrocker) PutMessage(topic string, message string, delay int64) {
	queueURL := mq.url + "/" + topic
	sqsInput := &sqs.SendMessageInput{
		QueueUrl:    &queueURL,
		MessageBody: &message,
	}
	sqsInput.SetDelaySeconds(delay)
	_, err := mq.client.SendMessage(sqsInput)
	if err != nil {
		panic(err)
	}
}

func (mq *messageBrocker) GetMessage(topic string) *string {
	var (
		err    error
		result *sqs.ReceiveMessageOutput
	)
	queueURL := mq.url + "/" + topic
	result, err = mq.client.ReceiveMessage(&sqs.ReceiveMessageInput{
		QueueUrl:            &queueURL,
		MaxNumberOfMessages: aws.Int64(1),
	})
	if err != nil {
		panic(err)
	}
	if len(result.Messages) == 0 {
		return nil
	}
	_, err = mq.client.DeleteMessage(&sqs.DeleteMessageInput{
		QueueUrl:      &queueURL,
		ReceiptHandle: result.Messages[0].ReceiptHandle,
	})
	if err != nil {
		panic(err)
	}

	return result.Messages[0].Body
}

func MessageBroker() *messageBrocker {
	if instanceMq == nil {
		instanceMq = &messageBrocker{}
		instanceMq.url = os.Getenv("SQS_HOST")
		instanceMq.key = os.Getenv("SQS_KEY")
		instanceMq.secret = os.Getenv("SQS_SECRET")
		err := instanceMq.buildClient()
		if err != nil {
			panic(err)
		}
	}
	return instanceMq
}
