package gateway

import (
	"encoding/json"
	"gitlab.com/pavelagp/gateway_framework/bank"
	"gitlab.com/pavelagp/gateway_framework/entities"
	ut "gitlab.com/pavelagp/gateway_framework/utilities"
	"log"
)

type registrar struct {
	activator string
	bank      bank.Bank
}

func newRegistrar(activator string, bank bank.Bank) *registrar {
	return &registrar{activator: activator, bank: bank}
}

func (r *registrar) run() {
	var (
		err error
	)
	log.Println("Registrar run successfully")
	for {
		request := entities.ProcessingRequest{}
		msg := ut.MessageBroker().GetMessage(r.activator)
		if msg == nil {
			continue
		}
		err = r.preparePayment(msg, &request)
		if err != nil {
			ut.Tracer().WriteTrace(request.Transaction.ID, 3010, "Error preparing payment "+err.Error())
			r.failPayment(&request)
			continue
		}
		err = r.bank.CreatePayment(&request)
		if err != nil {
			ut.Tracer().WriteTrace(request.Transaction.ID, 3050, "Error sending payment to gateway "+err.Error())
			r.failPayment(&request)
			continue
		}
		r.clarifyStatus(&request)
		ut.Tracer().WriteTrace(request.Transaction.ID, 3100, "Transaction registered on gateway")
	}
}

func (r *registrar) preparePayment(msg *string, request *entities.ProcessingRequest) (err error) {
	err = json.Unmarshal([]byte(*msg), &request)
	if err != nil {
		return
	}
	request.GatewayData = map[string]string{}
	paymentData := entities.PaymentData{}
	err = paymentData.GetFromStorage(request.Transaction.ID)
	if err != nil {
		return
	}
	request.PaymentData = paymentData.PaymentDetails
	return
}

func (r *registrar) failPayment(payment *entities.ProcessingRequest) {
	payment.Transaction.Status = entities.STATUS_FAIL
	paymentJsoned, _ := json.Marshal(payment.Transaction)
	ut.MessageBroker().PutMessage("finalizer", string(paymentJsoned), 0)
}

func (r *registrar) clarifyStatus(payment *entities.ProcessingRequest) {
	paymentJsoned, _ := json.Marshal(payment)
	ut.MessageBroker().PutMessage(r.activator+"_qualifier", string(paymentJsoned), 60)
}
