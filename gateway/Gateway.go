package gateway

import (
	"gitlab.com/pavelagp/gateway_framework/bank"
	ut "gitlab.com/pavelagp/gateway_framework/utilities"
)

func RunGateway(activator string, bank bank.Bank) {
	ut.MessageBroker()
	ut.Tracer()
	processor := newQualifier(activator, bank)
	go processor.run()
	initializer := newRegistrar(activator, bank)
	initializer.run()
}
