package gateway

import (
	"encoding/json"
	"gitlab.com/pavelagp/gateway_framework/bank"
	"gitlab.com/pavelagp/gateway_framework/bank/strategies"
	"gitlab.com/pavelagp/gateway_framework/entities"
	ut "gitlab.com/pavelagp/gateway_framework/utilities"
	"log"
)

type qualifier struct {
	activator string
	bank      bank.Bank
}

func newQualifier(activator string, bank bank.Bank) *qualifier {
	return &qualifier{activator: activator, bank: bank}
}

func (r *qualifier) run() {
	var (
		err error
	)
	log.Println("Qualifier run successfully")
	for {
		var bankResult *bank.ProcessingResult
		request := entities.ProcessingRequest{}
		msg := ut.MessageBroker().GetMessage(r.activator + "_qualifier")
		if msg == nil {
			continue
		}
		err = r.preparePayment(msg, &request)
		if err != nil {
			ut.Tracer().WriteTrace(request.Transaction.ID, 3110, "Error fetching data for status request "+err.Error())
			continue
		}
		bankResult, err = r.bank.ProcessPayment(&request)
		if err != nil {
			r.clarifyStatus(*msg)
			ut.Tracer().WriteTrace(request.Transaction.ID, 3150, "Error getting transaction status "+err.Error())
			continue
		}
		switch bankResult.Status {
		case strategies.IN_PROGRESS:
			r.clarifyStatus(*msg)
			ut.Tracer().WriteTrace(request.Transaction.ID, 3160, "Transaction IG PROGRESS taken")
		case strategies.FAIL:
			r.finalizePayment(&request, false)
			brs, _ := json.Marshal(bankResult)
			ut.Tracer().WriteTrace(request.Transaction.ID, 3170, string(brs))
		case strategies.SUCCESS:
			r.finalizePayment(&request, true)
			brs, _ := json.Marshal(bankResult)
			ut.Tracer().WriteTrace(request.Transaction.ID, 3200, string(brs))
		}
	}

}

func (r *qualifier) preparePayment(msg *string, request *entities.ProcessingRequest) (err error) {
	err = json.Unmarshal([]byte(*msg), &request)
	return err
}

func (r *qualifier) finalizePayment(payment *entities.ProcessingRequest, isSuccess bool) {
	if isSuccess {
		payment.Transaction.Status = entities.STATUS_SUCCESS
	} else {
		payment.Transaction.Status = entities.STATUS_FAIL
	}
	paymentJsoned, _ := json.Marshal(payment.Transaction)
	ut.MessageBroker().PutMessage("finalizer", string(paymentJsoned), 0)
}

func (r *qualifier) clarifyStatus(msg string) {
	ut.MessageBroker().PutMessage(r.activator+"_qualifier", msg, 30)
}
