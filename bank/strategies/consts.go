package strategies

type TxStrategy int

const (
	SUCCESS     TxStrategy = 3
	FAIL        TxStrategy = 2
	IN_PROGRESS TxStrategy = 1
)
