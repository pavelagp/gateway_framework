package bank

import "gitlab.com/pavelagp/gateway_framework/entities"

type Bank interface {
	CreatePayment(payment *entities.ProcessingRequest) (err error)
	ProcessPayment(payment *entities.ProcessingRequest) (*ProcessingResult, error)
}
