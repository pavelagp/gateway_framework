package bank

import (
	"github.com/pkg/errors"
	"gitlab.com/pavelagp/gateway_framework/entities"
)

type DefaultBank struct {
}

func (b *DefaultBank) CreatePayment(payment *entities.ProcessingRequest) (err error) {
	return errors.New("Not implemented yet")
}

func (b *DefaultBank) ProcessPayment(payment *entities.ProcessingRequest) (*ProcessingResult, error) {
	return nil, errors.New("Not implemented yet")
}
