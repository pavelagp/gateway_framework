package bank

import "gitlab.com/pavelagp/gateway_framework/bank/strategies"

type ProcessingResult struct {
	Id      int
	Status  strategies.TxStrategy
	Message string
	Rc      string
}
